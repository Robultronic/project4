\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{mathtools,bm,listings,graphicx,float,hyperref}
%opening
\title{Diffusion of neurotransmitters in the synaptic
cleft}
\author{Robert Olav Fauli}

\begin{document}

\maketitle

\begin{abstract}

\end{abstract}

\section{Introduction}

In the brain the dominant way from signals to go between neurons is by the diffusion of 
neurotransmitters across the synaptic cleft that seperates the membranes of the two neurons.

Since this is diffusion, we can model it by the use of the diffusion equation:

\begin{equation}
 \frac{\partial u}{\partial t} = D \nabla^2 u
\end{equation}

\begin{figure}[h]
 \centering
 \includegraphics[width = 0.8\textwidth]{thompsonB2000-p39}
 \caption{An illustration of the diffusion across the synaptic cleft (
From Thompson: “The Brain”, Worth
Publ., 2000).}
\end{figure}

Assuming that the neurotransmitters are released 
roughly equally on the ``presynaptic'' side and that
the cleft is roughly equally wide  across the whole terminal. We can
because of the large area of the cleft relative to its width assume
that the neurotransmitter concentration only varies across the cleft.
So we choose that direction to be the x-direction and we can write the
diffusion equation as:

\begin{equation}
 \frac{\partial u}{\partial t} = D \frac{\partial^2 u}{\partial x^2}
\end{equation}

We will approach the problem assuming that the concentration of transmitters
at the presynaptic cleftis always equal to one and the postsynaptic
receptors absorb all transmitters immediately (so the concentration is here
zero).

So we use the equation:

\begin{equation}
 \frac{\partial u}{\partial t} = \frac{\partial^2 u}{\partial x^2}
 \label{eq:diff}
\end{equation}

Which we will sometimes write as:

\begin{equation}
 u_t = u_{xx}
 \label{eq:disdiff}
\end{equation}


The initial conditions:

\begin{equation}
 u(x,0) = 0  \hspace{0.5cm}  0 < x < d
\end{equation}

And the boundary conditions:

\begin{equation}
 u(0,t) = 1  \hspace{0.5cm}  t > 0
\end{equation}

\begin{equation}
 u(d,t) = 0  \hspace{0.5cm}  t > 0
\end{equation}

We use $d = 1 $.
\\

To solve this system we will use three different algorithms, the explicit
forward Euler algorithm, the implicit backward Euler algorithm and the
Crank-Nicolson scheme.

\section{Solving the diffusion equation}
An obvious steady-state solution to our equation is:

\begin{equation}
 u_s (x) = 1 - x
\end{equation}

We can use this to define a new function $ v(s) = u(x) - u_s (x) $ that has
the boundary conditions $v(0) = v(d) = 0$. This is the initial condition for 
$v(x,t)$, just like $u(x)$ is the initial condition for $u(x,t)$.
When we solve the system numerically we need the boundary conditions to be 
zero to use that methods we have been taught in the lectures. So what we will
do is to solve for v(x,t) and the convert to u(x,t) by adding the steady state
solution to v(x,t).

% Write about why this makes it easier to solve bother numerically and on
% a closed form.

The closed form solution can be written as:

\begin{equation}
 v(x,t) = F(x)G(t)
\end{equation}

Using equation \ref{eq:diff} but with $v$ instead of $u$ we get:

\begin{equation}
 \frac{G'(t)}{G(t)} = \frac{F''(x)}{F(x)}
\end{equation}

LHS and RHS are dependent of different variables and must thus be constant.
We call this value $-\lambda^2$.

So we have two equations:

\begin{equation}
 G'(t) = -\lambda^2 G(t)
\end{equation}

\begin{equation}
 F''(x) + \lambda^2 F(x) = 0
\end{equation}

The first one is quite simple and solves for $u(t) = C \exp(-\lambda^2 t)$.
The second one is also standard and solves for $F(x) = A \sin(\lambda x) + B \cos(\lambda x)$.

With our boundary conditions we see that $B=0$ and $\lambda = n \pi $. We can absorb C into A and write:

\begin{equation}
 v(x,t) = A \sin(n \pi x) e^{-n^2 \pi^2 t}
\end{equation}


%Insert calculation here.

So we get:

\begin{equation}
 v(x) = v_c (x) + v_p (x) = x + c_1 \cos(\lambda x) + c_2 \sin(\lambda x) - 1
\end{equation}

This function ($v(x)$) is defined to have the boundary conditions
$v(0) = v(d=1) = 0$ and this gives us:

\begin{equation}
 v(0) = c_1 - 1 \Longrightarrow c_1 = 1
\end{equation}

\begin{equation}
 v(1) = 0 = \cos(\lambda) + c_2 \sin(\lambda)
\end{equation}

So we have $c_2 = -\cot(\lambda)$ which gives us an expression for $v(x)$:

\begin{equation}
 v(x) = x + \cos(\lambda x) + \cot(\lambda) \sin(\lambda x) - 1
\end{equation}

Thusly we get:

\begin{equation}
 u(x) = v(x) + 1 - x = \cos(\lambda x) + \cot(\lambda) \sin(\lambda x)
\end{equation}

Since we must have $u(1) = 0$ we must set lambda so that the cosine is zero
because the the cotangens will also be zero. So $\lambda = (2n-1) \pi /2 $.

This gives us the expression for $u(x,t)$:

\begin{equation}
 u(x,t) = u(x) u(t) = (x + 2 \cos(\frac{\pi}{2} x) - 1) 
 e^{-(\pi/2)^2 t}
\end{equation}


\section{Forward Euler Algorithm}

For forward Euler we have:

\begin{equation}
 u_t \approx \frac{u(x,t + \Delta t) - u(x,t)}{\Delta t} = \frac{u_{i,j+1} - u_{i,j}}{\Delta t}
\end{equation}

\begin{equation}
 u_{xx} \approx \frac{u(x+\Delta x,t) - 2 u(x,t) + u(x - \Delta x,t}{\Delta x^2}
\end{equation}

\begin{equation}
 u_{xx} \approx \frac{u_{i+1,j} - 2 u_{i,j} + u_{i-1,j}}{\Delta x^2}
\end{equation}

With the local approximation errors $O(\Delta t) $ and $O(\Delta x^2)$ these
come from the Taylor expansion one uses to derive these formulas.

We insert this in equation \ref{eq:disdiff}:

\begin{equation}
 u_{i,j+1} - u_{i,j} = \alpha ( u_{i+1,j} - 2 u_{i,j} + u_{i-1,j})
\end{equation}

Moving all the truncation errors to RHS we can see that the total local error
is $O(\Delta x^2) - O(\Delta t)$.
Where we have set $\alpha = \Delta t / \Delta x^2$. Now we solve for $u_{i,j+1}$:

\begin{equation}
 u_{i,j+1} = \alpha u_{i-1,j} + (1 - 2 \alpha) u_{i,j} + \alpha u_{i+1,j}
\end{equation}

If we specialize in the case where we have the boundaries 
$u_{0,j} = u_{n+1,j} = 0$ we can write this as $V_{j+1} = A V_j$ where:

\begin{equation}
A =
 \begin{bmatrix}
  1 - 2 \alpha & \alpha & 0 & 0\cdots \\
  \alpha & 1 - \alpha & \alpha & 0\cdots \\
  \cdots & \cdots & \cdots & \cdots \\
  0\cdots & 0\cdots & \alpha & 1 - 2\alpha
 \end{bmatrix}
 \hspace{0.5cm}
 V_j = 
 \begin{bmatrix}
  u_{1,j} \\
  u_{2,j} \\
  \cdots \\
  u_{n,j}
 \end{bmatrix}
\end{equation}

We then iterate of time by doing:

\begin{lstlisting}
 for (j = 0; j<timesteps; j++ )
 {
     V_{j+1} = A*V_j
 }
\end{lstlisting}

%Insert stability properties.

\section{Backward Euler algorithm}

For backward Euler we have:

\begin{equation}
 u_t \approx  \frac{u_{i,j} - u_{i,j-1}}{\Delta t}
\end{equation}

\begin{equation}
 u_{xx} \approx \frac{u_{i+1,j} - 2 u_{i,j} + u_{i-1,j}}{\Delta x^2}
\end{equation}

Inserting this into equation \ref{eq:disdiff} we get:

\begin{equation}
 u_{i,j} - u_{i,j-1} = \alpha ( u_{i+1,j} - 2 u_{i,j} + u_{i-1,j})
\end{equation}

\begin{equation}
 u_{i,j-1} = -\alpha u_{i+1,j} + (1-2\alpha) u_{i,j} -\alpha u_{i-1,j})
\end{equation}

These equations (going over all i) can if we again set the boundary conditions
to zero be written on matrix form:

\begin{equation}
 A V_j = V_{j-1}
\end{equation}

Where we in this case have the same $V_j$ as before, but a different A:

\begin{equation}
A =
 \begin{bmatrix}
  1 + 2 \alpha & -\alpha & 0 & 0\cdots \\
  -\alpha & 1 + \alpha & -\alpha & 0\cdots \\
  \cdots & \cdots & \cdots & \cdots \\
  0\cdots & 0\cdots & -\alpha & 1 + 2\alpha
 \end{bmatrix}
\end{equation}

The truncation errors remain the same as in forward Euler.

The algorithm then becomes:

\begin{lstlisting}
 for (j = 0; j<timesteps; j++ )
 {
     Solve for V_{j}: A*V_{j} = V_{j-1}
 }
\end{lstlisting}

This is a matrix equation with a tridiagonal matrix, for an algorithm for solving
this kind of equation see project 1.

%Insert text about stability proberties.

\section{Crank-Nicolson scheme}

See the lecture notes.

We are given the equations

\end{document}
