from scitools.std import *

#Sets the time and creates x-vector:
t = 0.21
x = linspace(0,1,10)

#Function inside the sum in u(x,t):
def f(x,t,n):
	n = float(n)
	return (1.0/(n*pi))*sin(n*pi*x)*exp(-n**2.0*pi**2.0*t)

#Plots the function for different n-values in the same plot:
plot(x,f(x,t,1), legend = "n = 1", xlabel = "x", ylabel = "u")
for i in range(2,6,1):
	print i
	hold('on')
	plot(x,f(x,t,i), legend = "n = %.0f" % i)
	
