#include <iostream>
#include <fstream>

using namespace std;

double** expscheme(double *uinit, double dx, double dt, int n, int timesteps);
double** impscheme(double *uinit, double dx, double dt, int n, int timesteps);
double** CNscheme(double *uinit, double dx, double dt, int n, int timesteps);
double* solve_tridiag(double a, double b, double c, double *V, int n);
double* tridiag_reducer(double **A, double *b, int n);
void vectofile(double *v, char* filename, int n);

ofstream ofile;

int main()
{
    //Setting stepsizes/distances and for how far int time we want to calculate:
    double dx = 1.0/11.0;
    double dt = 0.5*dx*dx;
    int n = 10;
    int timesteps = 100;

    double *uinit;
    uinit = new double[n];

    //Filling in initial values:
    for (int i = 1; i<n+1;i++){
        uinit[i-1] = i*dx - 1.0;
    }

    double** u = expscheme(uinit,dx,dt,n,timesteps);
    int t = 20; //Sets what timestep we want to write the data to files for.
    char name[] = { 'e', 'x', 'p', '.', 't', 'x', 't' };
    vectofile(u[t], name, n);

    //delete u;

    u = impscheme(uinit,dx,dt,n,timesteps);

    char name2[] = { 'i', 'm', 'p', '.', 't', 'x', 't' };
    vectofile(u[t], name2, n);

    u = CNscheme(uinit, dx, dt, n, timesteps);

    char name3[] = { 'c','n', '.', 't', 'x', 't' };
    vectofile(u[t], name3, n);

    for (int i = 0;i<timesteps;i++){
        delete u[i];
    }
    delete u;

    //Checking if the matrix equation solver works:
    /*double **M;
    M = new double*[4];
    M[0] = new double[4];
    M[1] = new double[4];
    M[2] = new double[4];
    M[3] = new double[4];

    for (int k = 0; k<4-1;k++){
        M[k][k] = 2.0;
        M[k+1][k] = - 1;
        M[k][k+1] = - 1;
    }
    //cout << "here" << endl;
    M[3][3] = 2.0;
    double *b;
    b = new double[4];
    b[0] = 0.0;
    b[1] = 1.0;
    b[2] = 2.0;
    b[3] = 3.0;

    double* v = tridiag_reducer(M,b,4);
    for (int i = 0; i<4;i++){
        cout << v[i] << endl;
    }*/
    //We should get v[] = {2, 4, 5, 4} if it works.

    cout << "main finished" << endl;
    return 0;
}

//Function that uses the explicit scheme to solve v_t = v_xx:
double** expscheme(double *uinit, double dx, double dt, int n, int timesteps)
{
    //Defines alpha, initializes the vector holding all data and adds initial conditions to it:
    double alpha = dt/(dx*dx);
    double **u;
    u = new double*[timesteps];
    for (int i=0;i<timesteps;i++){
        u[i] = new double[n];
    }
    for (int i = 0;i<n;i++){
        u[0][i] = uinit[i];
    }

    //Sets the elements for the tridiagonal matrix:
    double a = alpha;
    double b = 1.0 - 2.0*alpha;
    double c = alpha;
    double *Vnew;

    //Advances in time:
    for (int i = 0;i<timesteps-1;i++)
    {
        Vnew = solve_tridiag(a,b,c,u[i],n);
        for (int j = 1;j<n+1;j++){
            u[i+1][j] = Vnew[j];
        }
    }

    delete Vnew;

    cout << "expscheme done" << endl;
    return u;
}

//Function that uses the implicit scheme to solve v_t = v_xx:
double** impscheme(double *uinit, double dx, double dt, int n, int timesteps)
{
    //Defines alpha:
    double alpha = dt/(dx*dx);

    //Reserves memory for matrix A and the matrix holding all data:
    double **A, **u;
    A = new double*[n];
    u = new double*[timesteps];
    for (int i = 0;i<n;i++){
        A[i] = new double[n];
    }

    for (int i=0;i<timesteps;i++){
        u[i] = new double[n];
    }

    //Loading initial conditions into the data matrix:
    for (int i = 0;i<n;i++){
        u[0][i] = uinit[i];
    }

    double *Vnew, *ucopy;
    ucopy = new double[n];

    //Advances in time:
    for (int i = 0;i<timesteps-1;i++)
    {
        //Filling the matrix A with its elements and making a copy of the
        //data at the current time so it wont be changed by the solvers,
        //this is also why we refill A everytime (tried to create copies
        //but got memory errors):
        for (int k = 0; k<n-1;k++){
            A[k][k] = 1.0 + 2.0*alpha;
            A[k+1][k] = - alpha;
            A[k][k+1] = - alpha;
            ucopy[k] = u[i][k];
        }
        A[n-1][n-1] = 1.0 + 2.0*alpha;
        ucopy[n-1] = u[i][n-1];

        //Vnew is the data fir the next step:
        Vnew = tridiag_reducer(A,ucopy,n);
        for (int j = 0;j<n;j++){
            u[i+1][j] = Vnew[j];
            //cout << u[i][j] << endl;
        }
    }

    for (int i = 0; i<n;i++){
        //cout << u[0][i] << endl;
        delete A[i];
    }
    delete A;
    delete ucopy;
    cout << "Implicit scheme done" << endl;
    return u;
}

//Function that uses the CN scheme to solve v_t = v_xx:
double** CNscheme(double *uinit, double dx, double dt, int n, int timesteps)
{
    //Defines alpha and reserves memory for matrix B2 and the matrix holding all data:
    double alpha = dt/(dx*dx);
    double **B2, **u;

    B2 = new double*[n];
    u = new double*[timesteps];
    for (int i=0;i<n;i++){

        B2[i] = new double[n];
    }

    for (int i=0;i<timesteps;i++){
        u[i] = new double[n];
    }

    //Fill u with initial conditions:
    for (int i = 0;i<n;i++){
        u[0][i] = uinit[i];
    }

    //Setting elements for the first matrix:
    double a = 2.0 - 2.0*alpha;
    double b = + alpha;
    double c = + alpha;
    //Advancing in time:
    for (int i = 0;i<timesteps-1;i++){
        double* Vnew = solve_tridiag(a,b,c,u[i],n);

        //Filling the second matrix with elements for the same reason
        //we commented in the implicit scheme:
        for (int k = 0;k<n-1;k++){

            B2[k][k] = 2.0 + 2.0*alpha;
            B2[k+1][k] = - alpha;
            B2[k][k+1] = - alpha;
        }
        B2[n-1][n-1] = 2.0 + 2.0*alpha;

        u[i+1] = tridiag_reducer(B2, Vnew, n);
    }
    for (int i = 0; i<n; i++){

        delete B2[i];
    }

    delete B2;
    cout << "CN scheme done" << endl;
    return u;
}

double* solve_tridiag(double a, double b, double c, double *V, int n)
{
    double *Vnew;
    Vnew = new double[n];
    Vnew[0] = b*V[0] + c*V[1];
    for (int i = 1;i<n-1;i++){
        Vnew[i] = a*V[i-1] + b*V[i] + c*V[i+1];
    }
    Vnew[n-1] = a*V[n-2] + b*V[n-1];
    return Vnew;
}

double* tridiag_reducer(double **A, double *b, int n)
{
    //cout << "tridiag" << endl;
    double *v;
    v = new double[n];

    //I tried to copy the matrix and vector inside the function insted of refilling the matrices
    //So often, but got memory issues:
    /*double *br, **Ar;
    br = new double[n];
    for (int i = 0;i<n;i++){
        Ar[i] = new double[n];
    }

    br[0] = b[0];
    Ar[0][0] = A[0][0];

    for (int i = 1;i<n;i++){
        br[i] = b[i];
        Ar[i][i] = A[i][i];
        Ar[i-1][i] = A[i-1][i];
        Ar[i][i-1] = A[i][i-1];
    }*/

    //Row reduction:
    for(int i=1; i < n; i++) {
        b[i] = b[i] - b[i-1]*A[i][i-1]/A[i-1][i-1];
        A[i][i] = A[i][i] - (A[i][i-1]/A[i-1][i-1])*A[i-1][i];
        A[i][i-1] = 0;
    }

    //Solving for v:
    v[n-1] = b[n-1]/A[n-1][n-1];
    for(int i=n-2; i > -1; i--) {
        v[i] = (b[i] + v[i+1])/A[i][i];
    }
    /*for (int i = 0; i<n;i++){
        //cout << u[0][i] << endl;
        delete Ar[i];
    }
    delete Ar;
    delete br;*/
    return v;
}

//Function that writes selected data to a file with a chosen name:
void vectofile(double *v, char* filename, int n)
{
    ofile.open(filename);
    for (int i = 0; i<n;i++){
        ofile << v[i] << endl;
    }
    ofile.close();
    return;
}
