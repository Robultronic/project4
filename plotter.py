from scitools.std import *
import numpy as np


#The function inside the sum in u(x,t):
def f(x,t,n):
	n = float(n)
	return (1.0/(n*pi))*sin(n*pi*x)*exp(-n**2.0*pi**2.0*t)

#Function that reads in the data from file:
def vecread(filename):
	table = open(filename, 'r')
	rows = table.readlines()
	n = 1
	m = len(rows)
	#print m
	c = 0
	elements = np.zeros(m)
	for i in rows:
		row = i.split(' ')
		row = ''.join(row)
		#row = filter(lambda number: number.strip(), row)
		#print row
		for k in range(len(row)):
			elements[c] = float(row)
		c += 1
	return elements

#Applies the missing zero element on the front and the back:
yorig = vecread("imp.txt")
app = np.zeros(1)
ystep = np.append(yorig, app)
y = np.append(app,ystep)

#Creates x-vector and sets the time:
x = linspace(0,1,12)
t = 0.08

#Plots:
plot(x,y+1-x, legend = "Numerical", xlabel = "x", ylabel = "y")
hold('on')
plot(x,1-x - 2*(f(x,t,1)+f(x,t,2)), legend = "Analytical")
